import React from 'react';

import {Text, View, Dimensions,
    Image,Platform,StyleSheet,
    TouchableOpacity, AsyncStorage,
    StatusBar,
    TouchableWithoutFeedback} from 'react-native';
import {responsiveHeight,responsiveWidth,responsiveFontSize} from "./dimensions";
import {yellowColor} from "./Color";










const StatusBarHeader = ({ ...props}) => (
    <View style={styles.statusBar}>
        <StatusBar translucent backgroundColor={props.color} {...props} />
    </View>
);
const STATUSBAR_HEIGHT = Platform.OS === 'ios' ? responsiveHeight(3.2) : responsiveHeight(4);
const APPBAR_HEIGHT = Platform.OS === 'ios' ? 44 : 56;

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    statusBar: {

        height: STATUSBAR_HEIGHT,

    },
    appBar: {
        backgroundColor:'#B1B1B1',
        height: APPBAR_HEIGHT,
    },
    content: {
        flex: 1,
        backgroundColor: '#33373B',
    },
});
export default StatusBarHeader;