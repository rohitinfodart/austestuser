import {Image, Text, TouchableOpacity, View} from "react-native";
import React from "react";
import {lightGreyColor} from "./Color";
import {Icon} from "react-native-elements";

const Header=(props)=>{
    return(
        <View style={{backgroundColor:lightGreyColor,flexDirection:'row',
            height:60,width:'100%',}}>

            {props.name==='Selected Products'&&
            <View style={{alignItems:'center',justifyContent:'center'}}>
                <TouchableOpacity>
                    <Icon
                        raised
                        name='chevron-left'
                        type='font-awesome'
                        onPress={() => props.navigation.navigate('storeinfo',{fromProduct:true})} />
                </TouchableOpacity>
            </View>
            }

            {props.name==='Store Info'&&
            <View style={{alignItems:'center',justifyContent:'center'}}>
                <TouchableOpacity>
                    <Icon
                        raised
                        name='chevron-left'
                        type='font-awesome'
                        onPress={() => props.navigation.pop()} />
                </TouchableOpacity>
            </View>
            }




            <View style={{flex:1,alignItems:'center',justifyContent:'center'}}>
                <Text style={{width:'100%',fontWeight:'bold',textAlign:'center',color:'#000',fontSize:14}}>
                    {props.name}
                </Text>
            </View>
        </View>
    )
}
export default Header;

