export const yellowColor='#F9C42C';
export const lightGreyColor='#e5e5e2';
export const textInputBorderColor='#009688';
export const whiteColor='#FFFFFF';
export const blackColor='#000';
export const statusTextColor='#540000';
export const productNameTextColor='#0e189a';
export const priceText='#464646';