import React, { Component } from "react";
import {TextInput, StyleSheet, Dimensions, View, TouchableOpacity, Text} from "react-native";
import {yellowColor} from "../../common/Color";

const { height, width } = Dimensions.get("window");
export default class TouchableText extends Component {
    render() {
        const {
            name,backgroundColor,onPress,textColor
        } = this.props;
        return (

            <View style={{width:'100%',alignItems:name==='Forgot Password?'?'flex-end':'center',
                justifyContent:name==='Forgot Password?'?'flex-end':'center',
                marginTop:10,marginRight:name==='Forgot Password?'?20:0,}}>
                <TouchableOpacity  onPress={onPress} >
                    <Text style={{width:'100%',textAlign:'center',color:'#6de5d1',fontSize:12}}>
                        {name}
                    </Text>
                </TouchableOpacity>
            </View>


        );
    }
}

