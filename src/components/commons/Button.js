import React, { Component } from "react";
import {TextInput, StyleSheet, Dimensions, View, TouchableOpacity, Text} from "react-native";
import {yellowColor} from "../../common/Color";

const { height, width } = Dimensions.get("window");
export default class CustButton extends Component {
    render() {
        const {
            name,backgroundColor,onPress,textColor
        } = this.props;
        return (

            <View style={{justifyContent:'center',borderRadius:10,
                marginTop:30,width:'90%',height:50,backgroundColor:backgroundColor}}>
                <TouchableOpacity  onPress= {onPress} >
                    <Text style={{fontWeight: 'bold',width:'100%',textAlign:'center',color:textColor,fontSize:16}}>
                        {name}
                    </Text>
                </TouchableOpacity>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    textInput: {
        width: "90%",
        alignSelf: "center",
        borderBottomColor: "#c4c3c3",
        borderBottomWidth: 1.5,
        color: "#242222",
        height: 60,
        fontSize: 15,
        fontWeight: "bold"
    }
});
