import React, { Component } from 'react';
import { View, Text,FlatList,TextInput,TouchableOpacity,Picker } from 'react-native';
import StatusBarHeader from "../../common/StatusBar";
import {lightGreyColor} from "../../common/Color";
import Header from "../../common/header";
import { Icon } from 'react-native-elements';
import styles from './Styles';
export default class StoreData extends Component{
    constructor()
    {
        super();
        this.state={
            resultData:[],
            text:'',
            status:'all'
        }
    }
    componentDidMount()
    {
        this.setState({resultData:this.props.storeData})
    }
    SearchFilterFunction(text) {
        const newData = this.props.storeData.filter(function(item) {
            const itemData = item.tradingName ? item.tradingName.toUpperCase() : ''.toUpperCase();
            const textData = text.toUpperCase();
            return itemData.indexOf(textData) > -1;
        });
        this.setState({
            resultData: newData,
            text: text,
        });
    }

    selectedItem(itemValue,itemIndex)
    {
        if(itemValue==='pending')
        {
            const newData = this.props.storeData.filter(function(item) {
                if(item.status==='pending')
                {
                    return true;
                }
            });
            this.setState({status: itemValue,resultData:newData})
        }
        else if(itemValue==='verified')
        {
            const newData = this.props.storeData.filter(function(item) {
                if(item.status==='verified')
                {
                    return true;
                }
            });
            this.setState({status: itemValue,resultData:newData})
        }
        else
        {
            this.setState({status: itemValue,resultData:this.props.storeData})
        }

    }
    renderRow(item,index)
    {

            return(
                <TouchableOpacity onPress={() =>this.props.navigation.navigate('storeinfo',
                    {storeId:item.storeId})}>

                    <View>
                        <View style={styles.renderRowView}>
                            <Icon
                                raised
                                name='local-grocery-store'
                                type='material-icons'
                                color='#f50'
                                onPress={() => console.log('hello')} />
                            <View style={{marginLeft:10,justifyContent:'center'}}>
                                {item.tradingName &&  <Text style={styles.tradingText}>StoreName  {item.tradingName}</Text>}
                                {!item.tradingName &&  <Text style={styles.storeName}>{"Store Name"}</Text>}
                                <Text
                                    style={styles.statusText}>
                                    {item.status}
                                </Text>
                            </View>
                            <View style={styles.arrowView}>
                                <Icon
                                    raised
                                    name='chevron-right'
                                    type='font-awesome'
                                    color='#f50'
                                    onPress={() => console.log('hello')} />
                            </View>

                        </View>


                        <View style={styles.lineView}/>
                    </View>
                </TouchableOpacity>
            )
    }
    render() {



        return (
            <View style={{flex:1}}>
                <StatusBarHeader color={lightGreyColor}/>
                <Header name={"Stores"} navigation={this.props.navigation}/>
                <TextInput
                    style={styles.textInput}
                    onChangeText={text => this.SearchFilterFunction(text)}
                    value={this.state.text}
                    underlineColorAndroid="transparent"
                    placeholder="Search Here"
                />
                <Picker
                    selectedValue={this.state.status}
                    style={styles.pickerStyle}
                    onValueChange={(itemValue, itemIndex) =>
                        this.selectedItem(itemValue,itemIndex)
                    }>
                    <Picker.Item label="All" value="all" />
                    <Picker.Item label="Pending" value="pending" />
                    <Picker.Item label="Verified" value="verified" />
                </Picker>

                <FlatList
                    data={this.state.resultData}
                    enableEmptySections={true}
                    keyExtractor={(item, index) => index}
                    renderItem={({ item ,index}) => (
                        this.renderRow(item,index)

                    )}
                />
            </View>
        );
    }

}