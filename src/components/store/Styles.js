import {responsiveFontSize, responsiveHeight, responsiveWidth} from "../../common/dimensions";
import {
    blackColor,
    lightGreyColor,
    statusTextColor,
    textInputBorderColor,
    whiteColor
} from "../../common/Color";
import {  StyleSheet} from 'react-native';

const styles = StyleSheet.create({
    textInput:{height: responsiveHeight(7),
        borderWidth: 1,
        paddingLeft: responsiveHeight(1.5),
        margin:responsiveHeight(2),
        borderRadius:5,
        borderColor: textInputBorderColor,
        backgroundColor: whiteColor,},
    pickerStyle:{height: responsiveHeight(7), width: '90%',marginLeft:16,marginRight:16,borderRadius:5,
        borderColor: textInputBorderColor,backgroundColor:whiteColor},
    renderRowView:{margin:10,width:'100%',flexDirection:'row',
        alignItems:'center',height:responsiveHeight(12)},
    tradingText:{color:blackColor,fontSize:responsiveFontSize(2),},
    storeName:{color:blackColor,fontSize:responsiveFontSize(2),},
    statusText:{width:responsiveHeight(12),color:statusTextColor},
    arrowView:{marginRight:20,position:'absolute',right:0},
    lineView:{width:'100%',height:1,backgroundColor:lightGreyColor}

});
export default styles;