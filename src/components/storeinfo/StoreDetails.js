import React, { Component } from 'react';
import { View, Text, StyleSheet,Image,FlatList,ActivityIndicator,TextInput,TouchableOpacity,Alert } from 'react-native';
import StatusBarHeader from "../../common/StatusBar";
import {lightGreyColor, yellowColor} from "../../common/Color";
import Header from "../../common/header";
import {responsiveFontSize, responsiveHeight} from "../../common/dimensions";
import CustButton from "../commons/Button";
import styles from './Styles';
import { CheckBox } from 'react-native-elements'

let selectedItem=[];
export default class StoreDetails extends Component{
    constructor()
    {
        super();
        this.state={
            loading:true,
            resultData:{},

            checked:[],
            productListData:[],


        }
    }
    componentDidMount()
    {
        this.setState({resultData:this.props.storeInfoData,productListData:this.props.productListData})
    }
    componentWillReceiveProps(nextProps){
        if(nextProps.isChecked)
        {
            this.setState({productListData:nextProps.productListData})
        }

    }

    handleChange = (index,item) => {


        let array=this.state.productListData
        array[index]["isChecked"]=!array[index].isChecked
        this.props.checkedItemList(array)
        this.setState({ productListData:array });
    }
    renderData(item,index){
        return(
            <TouchableOpacity onPress={() =>this.props.navigation.navigate('storeinfo')}>

                <View>
                    <View style={styles.renderRowView}>
                        <View style={styles.checkBoxView}>
                            <CheckBox
                                center
                                onPress={() => this.handleChange(index,item)}
                                checked={item.isChecked} />
                        </View>
                        <View style={styles.textView}>
                            <Text style={styles.nameText}>{item.name}</Text>
                            <Text
                                style={styles.categoryText}>
                                {item.category}
                            </Text>
                            <Text
                                style={styles.priceText}>
                                Price: {item.priceCash}
                            </Text>

                        </View>




                    </View>

                    <View style={styles.lineView}/>
                </View>
            </TouchableOpacity>
        )
    }
    render() {
        return (
            <View style={{flex:1}}>
                <StatusBarHeader color={lightGreyColor}/>
                <Header name={"Store Info"} navigation={this.props.navigation}/>
                <View style={styles.storeDetailsView}>
                    {this.state.resultData.businessCategory &&
                    <Text style={styles.businessCategoryName}>{this.state.resultData.businessCategory}</Text> }
                    {this.state.resultData.fullAddress &&
                    <Text style={styles.addressText}>{this.state.resultData.fullAddress}</Text>}
                    {this.props.productListData.length===0 &&
                    <Text style={styles.noProductText}>No Products</Text>}




                    <FlatList
                        data={this.state.productListData}
                        extraData={this.state}
                        keyExtractor={(item, index) => index}
                        renderItem={({ item ,index}) => (
                            this.renderData(item,index)

                        )}
                    />
                </View>
                {
                    this.props.productListData.length>0 &&
                    <View style={styles.buttonView}>
                        <CustButton name={"Selected Products"}
                                    backgroundColor={yellowColor}
                                    textColor={'white'}
                                    onPress={() =>this.seeProducts()}/>
                    </View>
                }







            </View>
        );
    }
    seeProducts()
    {
        const { navigation } = this.props;
        navigation.navigate('productlist');

    }

}