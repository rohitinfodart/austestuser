import {responsiveFontSize, responsiveHeight, responsiveWidth} from "../../common/dimensions";
import {
    blackColor,
    lightGreyColor, priceText, productNameTextColor,
    statusTextColor,
    textInputBorderColor,
    whiteColor
} from "../../common/Color";
import {  StyleSheet} from 'react-native';

const styles = StyleSheet.create({
    storeDetailsView:{margin:10,justifyContent:'center',height:responsiveHeight(60)},
    businessCategoryName:{color:blackColor,fontSize:responsiveFontSize(2),},
    addressText:{color:blackColor,fontSize:responsiveFontSize(2),},
    buttonView:{alignItems:'center',justifyContent:'center'},
    renderRowView:{marginTop:10,marginBottom:10,height:responsiveHeight(15),flexDirection:'row',width:'100%'},
    checkBoxView:{justifyContent:'center'},
    textView:{marginLeft:10,justifyContent:'center'},
    nameText:{color:productNameTextColor,fontSize:responsiveFontSize(2),},
    categoryText:{width:responsiveHeight(12),color:blackColor},
    priceText:{width:responsiveHeight(12),color:priceText},
    lineView:{width:'100%',height:1,backgroundColor:lightGreyColor},
    noProductText:{textAlign:'center',color:'#000',fontSize:responsiveFontSize(4),},

});
export default styles;