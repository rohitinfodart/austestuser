import React, { Component } from 'react';
import { View, Text, StyleSheet,Image,FlatList,ActivityIndicator,TextInput,TouchableOpacity } from 'react-native';
import StatusBarHeader from "../../common/StatusBar";
import {lightGreyColor} from "../../common/Color";
import Header from "../../common/header";
import {responsiveFontSize, responsiveHeight} from "../../common/dimensions";
import {CheckBox, Icon} from "react-native-elements";
import styles from './Styles';
let selectedItem=[];
export default class ProductList extends Component{
    constructor()
    {
        super();
        this.state={
            loading:true,
            resultData:[],
            copyResultData:[],
            text:'',
            checked:[],
        }
    }
    componentDidMount()
    {

        this.setState({resultData:this.props.storeInfoData})

    }
    SearchFilterFunction(text) {
        const newData = this.props.storeData.filter(function(item) {
            const itemData = item.tradingName ? item.tradingName.toUpperCase() : ''.toUpperCase();
            const textData = text.toUpperCase();
            return itemData.indexOf(textData) > -1;
        });
        this.setState({
            resultData: newData,
            text: text,
        });
    }


    removeItem(item,index)
    {
        let array=this.props.storeInfoData;
        array[index]["isChecked"]=false
        const newData = array.filter(function(item) {
            if(item.isChecked)
            {
                return item
            }
            else
            {
                return false
            }
        });
        this.setState({ resultData:newData });
        this.props.checkedItemList(array)

    }
    renderRow(item ,index)
    {
        if(item.isChecked)
        {
            return(


                    <View>
                        <View style={styles.textView}>
                            <View style={styles.iconView}>
                                <Icon
                                    raised
                                    name='trash-o'
                                    type='font-awesome'
                                    color='#f50'
                                    onPress={() => this.removeItem(item,index)} />
                            </View>
                            <View style={styles.textSubView}>
                                <Text style={styles.nameText}>{item.name}</Text>
                                <Text
                                    style={styles.categoryText}>
                                    {item.category}
                                </Text>
                                <Text
                                    style={styles.priceText}>
                                    Price: {item.priceCash}
                                </Text>
                            </View>




                        </View>

                        <View style={styles.lineView}/>
                    </View>

            )
        }
        return (
            <View/>
        )

    }
    render() {


        return (
            <View style={styles.mainView}>
                <StatusBarHeader color={lightGreyColor}/>
                <Header name={"Selected Products"} navigation={this.props.navigation}/>
                {this.state.resultData.length<=0 &&  <Text
                    style={styles.noDataText}>
                    No Products
                </Text>}
                <FlatList
                    data={this.state.resultData}
                    enableEmptySections={true}
                    extraData={this.state}
                    keyExtractor={(item, index) => index}
                    renderItem={({ item ,index}) => (
                        this.renderRow(item,index)

                    )}
                />

            </View>
        );
    }

}