import {responsiveFontSize, responsiveHeight, responsiveWidth} from "../../common/dimensions";
import {
    blackColor,
    lightGreyColor,
    statusTextColor,
    textInputBorderColor,
    whiteColor
} from "../../common/Color";
import {  StyleSheet} from 'react-native';

const styles = StyleSheet.create({
   mainView:{flex:1},
    textView:{marginTop:10,marginBottom:10,height:responsiveHeight(12),flexDirection:'row',width:'100%'},
    iconView:{justifyContent:'center'},
    textSubView:{marginLeft:10,justifyContent:'center'},
    nameText:{color:'#0e189a',fontSize:responsiveFontSize(2),},
    categoryText: {width:responsiveHeight(12),color:'#000'},
    priceText:{width:responsiveHeight(12),color:'#464646'},
    noDataText:{textAlign:'center',fontSize: responsiveFontSize(3),color:'#464646'},
    lineView:{width:'100%',height:1,backgroundColor:lightGreyColor}

});
export default styles;