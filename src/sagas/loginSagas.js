import { put, call, fork, takeLatest } from 'redux-saga/effects';
import * as types from '../actions/types';
import * as loginApis from '../services/loginApis';
import {
    setStoreData,
    setUserInfo,
    setStoreInfoData,
    setProductList,
    setUpdateProductList
} from '../actions/loginActions';
import { setToken } from '../services/api';

export function* login(action) {
    try {
        const data = yield call(loginApis.login, action.params)
        action.onSuccess(data.data)
        setToken(data.data.token || '');
        yield put(setUserInfo(data.data.user))
    } catch (error) {
        action.onError(error)
    }
}

export function* watchLogin() {
    yield takeLatest(types.LOGIN, login)
}

export function* fetchUserInfo(action) {
    try {
        const data = yield call(loginApis.fetchUserInfo)
        action.onSuccess(data.data)
        yield put(setUserInfo(data.data))
    } catch (error) {
        action.onError(error)
    }
}

export function* fetchStoreData(action) {
    try {
        const data = yield call(loginApis.fetchStoreData)
        action.onSuccess(data.data)
        yield put(setStoreData(data.data))
    } catch (error) {
        action.onError(error)
    }
}


export function* fetchStoreInfoData(action) {
    try {
        const data = yield call(loginApis.fetchStoreInfoData,action.storeId)
        action.onSuccess(data.data)
        yield put(setStoreInfoData(data.data))
    } catch (error) {
        action.onError(error)
    }
}
export function* fetchProductList(action) {
    try {
        const data = yield call(loginApis.fetchProductList,action.storeId)
        action.onSuccess(data.data)
        yield put(setProductList(data.data))
    } catch (error) {
        action.onError(error)
    }
}

export function* updateProductList(action) {
    try {

        // action.onSuccess(action.data)
        yield put(setUpdateProductList(action.data))
    } catch (error) {
        // action.onError(error)
    }
}



export function* watchFetchUserInfo() {
    yield takeLatest(types.FETCH_USER_INFO, fetchUserInfo)
}

export function* watchFetchStoreData() {
    yield takeLatest(types.FETCH_STORE_DATA, fetchStoreData)
}
export function* watchFetchStoreInfoData() {
    yield takeLatest(types.FETCH_INFO_STORE_DATA, fetchStoreInfoData)
}

export function* watchUpdateProductList() {
    yield takeLatest(types.UPDATE_PRODUCT_LIST, updateProductList)
}

export function* watchFetchProductList() {
    yield takeLatest(types.FETCH_PRODUCT_LIST, fetchProductList)
}
