import { all } from 'redux-saga/effects';
import { watchLogin,
    watchFetchUserInfo,
    watchFetchStoreData,
    watchFetchStoreInfoData,watchFetchProductList,watchUpdateProductList} from '../sagas/loginSagas';
function* rootSaga() {
    yield all([
        watchLogin(),
        watchFetchUserInfo(),
        watchFetchStoreData(),
        watchFetchStoreInfoData(),
        watchFetchProductList(),
        watchUpdateProductList()

    ])
}
export default rootSaga