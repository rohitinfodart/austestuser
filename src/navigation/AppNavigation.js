import {
    createStackNavigator,
    createAppContainer
} from 'react-navigation';
import StoreContainer from '../containers/stores/StoreContainer';
import StoreInfoContainer from '../containers/storeinfo/StoreInfoContainer';
import ProductListContainer from '../containers/productlist/ProductListContainer';
import Splash from '../containers/other/Splash';


const AppStackNavigator = createStackNavigator(
    {
        splash: { screen: Splash },
        store: { screen: StoreContainer },
        storeinfo: { screen: StoreInfoContainer },
        productlist: { screen: ProductListContainer },


    }, {
        // see next line
        headerMode: 'none',
    })
const App = createAppContainer(AppStackNavigator);

export default App