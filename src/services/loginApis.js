import { api } from './api';

export const login = (params) => {
    return api.post('/login', params);
}
export const fetchStoreData = () => {
    // return api.get('/?i=onions,garlic&q=omelet&p=3');
    return api.get('/get-all-stores');
}

export const fetchStoreInfoData = (storeId) => {
    // return api.get('/?i=onions,garlic&q=omelet&p=3');
    return api.get('/get-store?storeId='+storeId);
}
export const fetchProductList = (storeId) => {
    // return api.get('/?i=onions,garlic&q=omelet&p=3');
    return api.get('/get-store-products?storeId='+storeId);
}

export const fetchUserInfo = () => {
    return api.get('/getUserInfo');
}