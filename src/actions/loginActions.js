import {
    LOGIN,
    SET_USER_INFO,
    FETCH_USER_INFO,
    FETCH_STORE_DATA,
    SET_STORE_DATA,
    FETCH_INFO_STORE_DATA,
    SET_INFO_STORE_DATA, FETCH_PRODUCT_LIST, SET_PRODUCT_LIST, UPDATE_INFO_STORE_DATA,
    UPDATE_PRODUCT_LIST,
    SET_UPDATE_PRODUCT_LIST
} from './types';
export const login = (params, onSuccess, onError) => ({
    type: LOGIN,
    params,
    onSuccess,
    onError
})
export const setUserInfo = (data) => ({
    type: SET_USER_INFO,
    data,
})
export const setStoreData = (data) => ({
    type: SET_STORE_DATA,
    data,
})
export const setStoreInfoData = (data) => ({
    type: SET_INFO_STORE_DATA,
    data,
})

export const setProductList = (data) => ({
    type: SET_PRODUCT_LIST,
    data,
})

export const setUpdateProductList = (data) => ({
    type: SET_UPDATE_PRODUCT_LIST,
    data,
})
export const fetchUserInfo = (onSuccess, onError) => ({
    type: FETCH_USER_INFO,
    onSuccess,
    onError
})

export const fetchStoreData = (onSuccess, onError) => ({
    type: FETCH_STORE_DATA,
    onSuccess,
    onError
})

export const fetchStoreInfoData = (storeId,onSuccess, onError) => ({
    type: FETCH_INFO_STORE_DATA,
    storeId,
    onSuccess,
    onError
})

export const updateProductList = (data,onSuccess, onError) => ({
    type: UPDATE_PRODUCT_LIST,
    data,
    onSuccess,
    onError
})
export const fetchProductList = (storeId,onSuccess, onError) => ({
    type: FETCH_PRODUCT_LIST,
    storeId,
    onSuccess,
    onError
})