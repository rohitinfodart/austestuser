import React, { Component } from 'react';
import { View,ActivityIndicator } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as loginActions from "../../actions/loginActions";
import {lightGreyColor} from "../../common/Color";
import StatusBarHeader from "../../common/StatusBar";
import Header from "../../common/header";
import StoreData from "../../components/store/StoreData";
import StoreDetails from "../../components/storeinfo/StoreDetails";
class StoreInfoContainer extends Component{
    constructor()
    {
        super();
        this.state={
            loading:true,
            resultData:{},
            productListData:[],
        }
    }

    componentDidMount() {


        if( this.props.navigation.state.params && this.props.navigation.state.params.fromProduct)
        {
            this.setState({loading:false})
        }
        else
        {


            setTimeout(() => {
                this.props.actions.storeInfoData.fetchStoreInfoData(this.props.navigation.state.params.storeId,
                    this.onSuccess, this.onError)
            }, 3000);
        }

    }
    onSuccess = (data) => {
        if(data.products)
        {

            let myArray=data.products;

            for (let i = 0; i <myArray.length ; i++) {
                myArray[i]["isChecked"]=false;
            }

            this.setState({loading:false,productListData:myArray})

        }
        else
        {
            this.setState({resultData:data.store})
            this.props.actions.productList.fetchProductList(this.props.navigation.state.params.storeId,
                this.onSuccess, this.onError)

        }




    }


    onError = (error) => {
        this.setState({loading:false})

    }

    render() {
        let isChecked=false;
        if( this.props.navigation.state.params && this.props.navigation.state.params.fromProduct)
        {
            isChecked=true;
        }

        if(this.state.loading)
        {
            return (
                <View style={{flex:1}}>
                    <StatusBarHeader color={lightGreyColor}/>
                    <Header name={"Store Info"} navigation={this.props.navigation}/>
                    <ActivityIndicator size={"large"}/>
                </View>
            )
        }
        else if(this.props.productList && this.props.productList.length>0)
        {
            return (
                <StoreDetails
                    navigation={this.props.navigation}
                    storeInfoData={this.props.resultData}
                    checkedItemList={this.checkedItemList.bind(this)}
                    productListData={this.props.productList}
                isChecked={isChecked}/>
            );
        }


        return (
            <StoreDetails
                navigation={this.props.navigation}
                storeInfoData={this.state.resultData}
                checkedItemList={this.checkedItemList.bind(this)}
                productListData={this.state.productListData}/>
        );
    }
    checkedItemList(data)
    {
        this.props.actions.updateProductList.updateProductList(data,this.onSuccess, this.onError)

    }

}
const mapStateToProps = (state) => {
    return {
        productList:state.login.productList
    }
}
const mapDispatchToProps = (dispatch) => {
    return {
        actions: {
            storeInfoData: bindActionCreators(loginActions, dispatch),
            productList: bindActionCreators(loginActions, dispatch),
            updateProductList:bindActionCreators(loginActions,dispatch)
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(StoreInfoContainer);