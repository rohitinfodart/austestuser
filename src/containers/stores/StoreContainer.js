import React, { Component } from 'react';
import { View,ActivityIndicator } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as loginActions from "../../actions/loginActions";
import {lightGreyColor} from "../../common/Color";
import StatusBarHeader from "../../common/StatusBar";
import Header from "../../common/header";
import StoreData from "../../components/store/StoreData";
class StoreContainer extends Component{
    constructor()
    {
        super();
        this.state={
            loading:true,
            resultData:[],
        }
    }
    componentDidMount() {
        setTimeout(() => {
            this.props.actions.storeData.fetchStoreData(this.onSuccess, this.onError)
        }, 3000);
    }
    onSuccess = (data) => {
        this.setState({loading:false,resultData:data.stores})
    }

    onError = (error) => {
        this.setState({loading:false})

    }

    render() {
        if(this.state.loading)
        {
            return (
                <View style={{flex:1}}>
                    <StatusBarHeader color={lightGreyColor}/>
                    <Header name={"Stores"} navigation={this.props.navigation}/>
                    <ActivityIndicator size={"large"}/>
                </View>
            )
        }


        return (
           <StoreData
               navigation={this.props.navigation}
               storeData={this.state.resultData}/>
        );
    }

}
const mapStateToProps = (state) => {
    return {}
}
const mapDispatchToProps = (dispatch) => {
    return {
        actions: {
            storeData: bindActionCreators(loginActions, dispatch)
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(StoreContainer);