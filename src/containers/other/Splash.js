import React, { Component } from 'react';
import { View, Text, StyleSheet,Image } from 'react-native';
import { getToken, clearToken } from '../../utils/storage';
import * as loginActions from '../../actions/loginActions';
import { NavigationActions, StackActions } from 'react-navigation';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import StatusBarHeader from "../../common/StatusBar";
import {yellowColor} from "../../common/Color";

class Splash extends Component {

    componentDidMount() {
        setTimeout(() => {
            this.checkSignInStatus();
        }, 3000);
    }



    checkSignInStatus() {
        getToken().then((token) => {
            if (token && token.length > 0) {
                this.props.actions.user.fetchUserInfo(this.onSuccess, this.onError)
            } else {
                const { navigation } = this.props;
                const resetAction = StackActions.reset({
                    index: 0,
                    actions: [
                        NavigationActions.navigate({ routeName: 'store' }),
                    ],
                });
                navigation.dispatch(resetAction);
            }
        });
    }
    render() {
        return (
            <View style={styles.container}>
                <StatusBarHeader color={yellowColor}/>
                <Image
                    style={{width:100,height:100}}
                    source={require('../../images/fork.png')}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: yellowColor,
    },
})

const mapDispatchToProps = (dispatch) => ({
    actions: {
        user: bindActionCreators(loginActions, dispatch)
    }
})
export default connect(null, mapDispatchToProps)(Splash)


