import React, { Component } from 'react';
import { View,ActivityIndicator } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as loginActions from "../../actions/loginActions";
import {lightGreyColor} from "../../common/Color";
import StatusBarHeader from "../../common/StatusBar";
import Header from "../../common/header";
import StoreData from "../../components/store/StoreData";
import StoreDetails from "../../components/storeinfo/StoreDetails";
import ProductList from "../../components/productlist/ProductList";
class ProductListContainer extends Component{
    constructor()
    {
        super();
        this.state={
            loading:true,
            resultData:[],

        }
    }



    onSuccess = (data) => {
        // this.setState({loading:false,resultData:data.products})
        console.log("ProductListSuccess",data)

    }

    onError = (error) => {
        console.log("ProductListError",error)
        // this.setState({loading:false})

    }

    render() {
        console.log("ProductListError",this.props.productList)

        return (
            <ProductList
                navigation={this.props.navigation}
                checkedItemList={this.checkedItemList.bind(this)}
                storeInfoData={this.props.productList}/>
        );
    }
    checkedItemList(data)
    {
        console.log("GetSelectedDaatattatat",data)
        this.props.actions.updateProductList.updateProductList(data,this.onSuccess, this.onError)

    }

}
const mapStateToProps = (state) => {
    console.log("ProductListtststtststtststs",state)
    return {
        productList:state.login.productList
    }
}
const mapDispatchToProps = (dispatch) => {
    return {
        actions: {
            updateProductList:bindActionCreators(loginActions,dispatch)
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ProductListContainer);